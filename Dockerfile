FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
#Installation and program and packages
RUN apt-get update -y && \  
    apt-get install -y --no-install-recommends \
        software-properties-common && \
    add-apt-repository ppa:chrberger/libcluon && \
    apt-get update -y && \
    apt-get upgrade -y && \
    apt-get dist-upgrade -y && \
    apt-get install -y --no-install-recommends \
        build-essential \
        python3-protobuf \
        python3-sysv-ipc \
        python3-numpy \
        python3-opencv \
        protobuf-compiler \
        libcluon && \
    apt-get clean

#Adds the current executable into opt/sources
ADD . /opt/sources
WORKDIR /opt/sources
# Set default directory in image
RUN make
# Executes the code
#ENTRYPOINT ["tail", "-f" "/dev/null"]
ENTRYPOINT ["python3", "/opt/sources/myApplication.py"]
# ENTRYPOINT ["echo","test","/opt/sources/delayFile.py"]
		

# Provat "bash", innan entry point.


#W hen the image is executed the file myApplication in /# opt/sources will be executed
